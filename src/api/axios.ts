import axios from "axios";

const api = axios.create({
  baseURL: 'https://deploy-backend-twitter.herokuapp.com/api/',
  timeout: 1000,
  headers: {
    'x-token': localStorage.getItem('token') || ''
  }
});

export default api;