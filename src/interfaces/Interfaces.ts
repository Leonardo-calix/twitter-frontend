export interface responseAxios {
    config: any
    data: any
    headers: any
    request: any
    status: number
    statusText: string
}

export interface User {
    name: string,
    userName : string,
    lastName: string,
    email: string,
    phone: string
}

export interface Tweest {
    comment: String,
    user: User,
    date: Date,
    img:String,
    likes: Number,
}

export interface Hashtag {
    user: User,
    comment: String,
    img:String,
    date: Date
}

export interface Trend {
    title: String,
    hashtag: Hashtag,
    date: Date,
    commentCount: Number,
}

export interface PropsCard {
    tweest: Tweest
}


export interface PropsCardTrends {
    trend: Trend
}




