
import React, { useState } from 'react'
import { useHistory } from 'react-router-dom';
import { validateEmail, validatePassword } from '../services/service';

const Login = () => {

    const history = useHistory();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [valido, setValido] = useState<boolean>(false);


    const validateCorreo = async () => {
        const res: any = await validateEmail(email);

        if (res.status === 200) {

            console.log(res);

            setValido(true);
        }
    }

    const validatePasswordUser = async () => {
        const res: any = await validatePassword({ email, password });

        if (res.status === 200) {

            console.log(res.data.token);

            localStorage.setItem('token', res.data.token);

            history.push("/")

        }
    }



    return (
        <div className="bg-light w-100 d-inline-block vh-100 border rounded" >
            <div className="d-flex justify-content-center mb-5 p-5">
                <div className="align-items-center mt-5 p-5 " >
                    {
                        !valido &&

                        <div className="p-5" style={{ background: 'white' }}>
                            <h3 className="" >Inicia sesión en Twitter</h3> <br />

                            <button style={{ color: 'white' }} className="btn  btn-secondary rounded m-2 btn-block rounded-pill" >Inicia sesión con Google</button>
                            <button style={{ color: 'white' }} className="btn  btn-secondary rounded m-2 btn-block rounded-pill" >Inicia sesión con Apple</button>

                            <div className="form-group m-2">
                                <br />
                                <input value={email} onChange={(e) => setEmail(e.target.value)} type="email" className="form-control m-2" id="exampleInputEmail1" placeholder="Correo electronico" />
                                <br />
                            </div>
                            <button onClick={validateCorreo} style={{ background: 'black', color: 'white' }} className="btn btn-default rounded m-2 btn-block rounded-pill" >Siguiente</button><br />
                            <button style={{ background: 'white', color: 'black' }} className="btn btn-default rounded m-2 btn-block rounded-pill" >¿Olvidaste tu contraseña?</button>
                        </div>
                    }

                    {
                        valido &&
                        <div className="p-5" style={{ background: 'white' }}>
                            <h3 >Ingrese la contraseña</h3> <br />
                            <div className="form-group m-2">
                                <br />
                                <input value={password} onChange={(e) => setPassword(e.target.value)} type="password" className="form-control m-2" id="exampleInputEmail1" placeholder="Ingrese la contraseña" />
                                <br />
                            </div>
                            <button onClick={validatePasswordUser} style={{ background: 'black', color: 'white' }} className="btn btn-default rounded m-2 btn-block rounded-pill" >Siguiente</button><br />
                        </div>
                    }
                </div>
            </div>
        </div>

    )
}

export default Login
