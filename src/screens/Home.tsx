import Navbar from '../components/Navbar';
import Sidebar from '../components/Sidebar';
import Trending from '../components/Trending';
import Tweets from './Tweets';

const Home = () => {
  
  return (

    <div className="row">
      <div className="col-md-3">
        <Sidebar />
      </div>
      <div className="col-md-6 p-3">
        <Navbar />
        <Tweets />
      </div>

      <div className="col-md-3 bg-light rounded mt-3">
        <div className="mt-5 mb-2 p-4">
          <input className="form-control mt-3" type="text" placeholder="search" />
          <br />
          <h4>Trendinds for you</h4>
        </div>
        <Trending />
      </div>
    </div>
  )

}

export default Home
