import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import Card from "../components/Card";
import { Tweest } from "../interfaces/Interfaces";
import { getComments, sendCommentApi } from "../services/service";

const Tweets = () => {

    const history = useHistory();

    const [tweests, setTweests] = useState<Tweest[]>([]);
    const [comment, setComment] = useState<string>('');
    const [img, setImg] = useState<string>('');

    const sendComment = async () => {
        const res: any = await sendCommentApi({ comment, img });
        if (res.status === 201) {
            setComment('');
            setImg('');
            getComment();
        }
    }

    const getComment = async () => {
        const res: any = await getComments();

        if (res.status === 200) {
            setTweests(res.data.reverse());
        } else {
            history.push("/login");
        }
    }

    useEffect(() => {
        getComment();
        console.clear();
        
    }, []);


    return (
        <div>
            <div className="mb-3 mt-3" >
                <div className="card text-center">
                    <div className="card-body p-2">
                        <div className="row">
                            <div className="col-2">
                                <img className="img-fluid rounded-circle " width="50px" height="50px" src="https://us.123rf.com/450wm/thesomeday123/thesomeday1231712/thesomeday123171200009/91087331-icono-de-perfil-de-avatar-predeterminado-para-hombre-marcador-de-posici%C3%B3n-de-foto-gris-vector-de-ilu.jpg?ver=6" alt="" />

                            </div>
                            <div className="col-10">
                                <textarea rows={4} value={comment} placeholder="What's happening ?" onChange={(e) => setComment(e.target.value)} className="border-0 form-control" ></textarea>
                            </div>
                        </div>
                    </div>
                    <div className="card-footer text-muted">
                        <div className="row ">
                            <div className="col-10 ">
                                <div className="btn-group float-left" role="group" aria-label="Basic example">
                                    <button data-toggle="modal" data-target="#exampleModal" type="button" className="btn"><i className="bi bi-card-image"></i></button>
                                    <button type="button" className="btn"> <i className="bi bi-emoji-smile"></i></button>
                                    <button type="button" className="btn"><i className="bi bi-calendar-check"></i></button>
                                    <button type="button" className="btn"> <i className="bi bi-graph-up-arrow"></i> </button>
                                    <button type="button" className="btn"> <i className="bi bi-gift"></i></button>
                                </div>
                            </div>
                            <div className="col-2">
                                <button onClick={sendComment} disabled={comment.trim().length === 0} className="btn btn-sm btn-primary" >Tweets</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {
                tweests.length > 0 &&
                tweests.reverse().map((tweest: any, key: number) => {
                    return (
                        <Card key={key} tweest={tweest} />
                    )
                })
            }

            <div>
                <div className="modal fade" id="exampleModal" tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Image</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <input className="form-control" value={img} onChange={(e) => setImg(e.target.value)} type="text" placeholder="Url imagen" />
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Tweets
