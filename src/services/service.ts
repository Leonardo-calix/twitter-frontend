import api from "../api/axios"

export const validateEmail = async (email: string) => {
    try {
        const response = await api.post('/users/validar-email', { email });
        return response;

    } catch (error) {
        return error;
    }
}

export const validatePassword = async (data: { email: string, password: string }) => {
    try {
        const response = await api.post('/users/validar-password', data);
        return response;

    } catch (error) {
        return error;
    }
}

export const sendCommentApi = async (data: { comment: string,  img: string}) => {
    try {
        const response = await api.post('/tweest', data);
        return response;

    } catch (error) {
        return error;
    }
}

export const getComments = async () => {
    try {
        const response = await api.get('/tweest');
        return response;

    } catch (error) {
        return error;
    }
}

export const getTrends = async () => {
    try {
        const response = await api.get('/trends');
        return response;

    } catch (error) {
        return error;
    }
}
