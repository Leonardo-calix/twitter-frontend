import { useEffect, useState } from 'react';
import { Route, Switch, HashRouter } from 'react-router-dom';
import Home from './screens/Home';
import Login from './screens/Login';

const App = () => {

  const [loading, setLoading] = useState(false);
  


  useEffect(() => {
    setLoading(true);
  }, [])

  if (!loading) {
    return (<h5>Cargando</h5>)
  }


  return (
    <HashRouter>
      <Switch>
        <Route exact path="/login" component={Login} />
        <Route exact path="/" component={Home} />
      </Switch>
    </HashRouter>
  )
}

export default App
