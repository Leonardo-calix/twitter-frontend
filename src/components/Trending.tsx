import React, { useEffect, useState } from 'react'
import { Trend } from '../interfaces/Interfaces';
import { getTrends } from '../services/service';
import CardTrends from './CardTrends';

const Trending = () => {

    const [trends, setTrends] = useState<Trend[]>([]);

    useEffect(() => {
        getTrendsFromApi();
    }, []);

    const getTrendsFromApi = async () => {
        const res: any = await getTrends();

        if (res.status === 200) {
            setTrends(res.data);
        }
    }


    return (
        <div>

            <div className="container-fluid m-2 border-0" >
                {
                    trends.map((trend: Trend, key: number) => {
                        return (
                            <CardTrends key={key} trend={trend} />
                        )
                    })
                }
            </div>
        </div>
    )
}

export default Trending
