
import { PropsCard } from '../interfaces/Interfaces';

const Card = (props: PropsCard) => {

    const { tweest } = props;

    return (
        <div className="card text-center mt-2 mb-2">
            <div className="card-header">
                <div className="row float-left">
                    <div className="col-3 col-md-3 col-lg-3 m-0 p-0">
                        <img className="img-fluid rounded-circle " width="60px" height="60px" src="https://us.123rf.com/450wm/thesomeday123/thesomeday1231712/thesomeday123171200009/91087331-icono-de-perfil-de-avatar-predeterminado-para-hombre-marcador-de-posici%C3%B3n-de-foto-gris-vector-de-ilu.jpg?ver=6" alt="" />
                    </div>
                    <div className="col-9 col-md-9 col-lg-9 m-0 p-2">
                        <p>{tweest.user.name} <span className="text-muted" >@{tweest.user.userName} </span>  - 21Min </p>
                        <p className="float-left lead" >{tweest.comment} </p>
                    </div>
                </div>
            </div>
            {
                tweest.img &&
                <div className="card-body m-0 p-0">
                    <img className="img-fluid" src={String(tweest.img)} alt="img" />
                </div>
            }

        </div>
    )
}

export default Card
