import { PropsCardTrends } from "../interfaces/Interfaces"

const CardTrends = (props: PropsCardTrends) => {

    const { trend } = props;

    return (
        <div>
            <div className="card rounded border-0 bg-light m-2">
                <div className="card-body m-2 p-0">
                    <div className="row">
                        <div className="col-sm-8 col-md-8">
                            <p className="m-0 p-1" >#{trend.title}  </p>
                            <p className="font-weight-bold m-0 p-1" >#{trend.hashtag.comment} </p>
                            <p className="m-0 p-1" >{trend.commentCount} Tweets</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CardTrends
