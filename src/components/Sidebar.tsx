import { Link } from 'react-router-dom'

const Sidebar = () => {
    return (
        <div className="m-5" style={{ position: 'fixed' }} >
            <img className="img-fluid" width={60} height={60} src="https://nintendo-power.com/wp-content/uploads/2021/05/Twitter-logo-subject-history-download-etc.png" alt="logo-twiter" />
            <ul className="list-group border-0 mt-5 mb-2 border-0 text-center">
                <li style={{ fontSize: 20, border: 'none' }} className="btn-sidebar font-weight-bold list-group-item d-flex justify-content-between align-items-center b-0">
                    <Link to=""><span><i className="bi bi-house-door-fill m-0"></i> Home</span></Link>
                </li>
                <li style={{ fontSize: 20, border: 'none' }} className=" btn-sidebar font-weight-light list-group-item d-flex justify-content-between align-items-center b-0">
                    <Link to="/explore"><span><i className="bi bi-search"></i> Explore</span></Link>
                </li>
                <li style={{ fontSize: 20, border: 'none' }} className=" btn-sidebar font-weight-light list-group-item d-flex justify-content-between align-items-center b-0">
                    <Link to="/explore"><span><i className="bi bi-music-note-beamed m-1"></i> Notifications</span></Link>
                </li>
                <li style={{ fontSize: 20, border: 'none' }} className=" btn-sidebar font-weight-light list-group-item d-flex justify-content-between align-items-center b-0">
                    <Link to="/expore"><span><i className="bi bi-envelope"></i> Messages</span></Link>
                </li>
                <li style={{ fontSize: 20, border: 'none' }} className=" btn-sidebar font-weight-light list-group-item d-flex justify-content-between align-items-center b-0">

                    <Link to="/expore"><span><i className="bi bi-bookmark-check-fill"></i> Bookmarks</span></Link>
                </li>
                <li style={{ fontSize: 20, border: 'none' }} className=" btn-sidebar font-weight-light list-group-item d-flex justify-content-between align-items-center b-0">

                    <Link to="/expore"><span><i className="bi bi-card-list"></i> Lists</span></Link>
                </li>
                <li style={{ fontSize: 20, border: 'none' }} className=" btn-sidebar font-weight-light list-group-item d-flex justify-content-between align-items-center b-0">
                    <Link to="/expore"><span><i className="bi bi-person-fill"></i> Profile</span></Link>
                </li>
                <li style={{ fontSize: 20, border: 'none' }} className=" btn-sidebar font-weight-light list-group-item d-flex justify-content-between align-items-center b-0">
                    <Link to="/expore"  > <span><i className="bi bi-plus-square-fill"></i> More</span> </Link>
                </li>
                <li style={{ fontSize: 20, border: 'none' }} className=" btn-sidebar font-weight-light list-group-item d-flex justify-content-between align-items-center b-0">
                    <button className="btn btn-primary btn-sm" >Tweet</button>
                </li>
            </ul>
        </div>


    )
}

export default Sidebar
